<?php

return [
    // category icons
    'icon_home' => 'fas fa-home',
    'icon_entertainment' => 'fas fa-star',
    'icon_chakdoat' => 'fas fa-heart',
    'icon_social' => 'fas fa-share-alt-square',
    'icon_beauty' => 'fab fa-shopify',
    'icon_abstract' => 'fas fa-question-circle',
    'icon_pr' => 'fas fa-bullhorn',
    'icon_radio' => 'fas fa-microphone-alt',

    // background
    'bg_pink' => '.bg-pink',

];
