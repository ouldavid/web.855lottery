@extends('layouts.master')

@section('title', ucfirst(Route::currentRouteName()))

@section('css')
    <!-- slick slider -->
    <link rel="stylesheet" href="{{ asset('plugins/slick/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/slick/slick-theme.css') }}">
    <!-- page css -->
    <link rel="stylesheet" href="{{ asset('css/'.strtolower(Route::currentRouteName()).'.css?v='.$version) }}">
@endsection

@section('banner')
    <div class="bg-pink">
        <div class="container-fluid container-md full-screen py-3">
            <div class="row">
                <div class="col-md-7 pr-1">
                    <div class="row">
                        <div class="col-md-12 mb-2">
                            <div class="banner-slide">
                                <a href="#">
                                    <img class="img-fluid" src="{{ asset('img/banner_01.jpg') }}" alt="" />
                                </a>
                            </div>
                        </div>
                        <div class="col-md-6 pr-1">
                            <a href="#">
                                <img class="img-fluid" src="{{ asset('img/banner_01.jpg') }}" alt="" />
                            </a>
                        </div>
                        <div class="col-md-6 pl-1">
                            <a href="#">
                                <img class="img-fluid" src="{{ asset('img/banner_01.jpg') }}" alt="" />
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-md-5 pl-1">
                    <div class="row">
                        <div class="col-12 mb-11">
                            <img class="img-fluid" src="{{ asset('img/banner_subradio_01.jpg') }}" alt="" />
                        </div>
                        <div class="col-12">
                            <img class="img-fluid" src="{{ asset('img/banner_subradio_01.jpg') }}" alt="" />
                        </div>
                    </div>
                </div>
            </div><!-- .row -->
        </div><!-- .container -->
    </div><!-- background -->

@endsection

@section('content')
    <div class="container" style="height: 1000px">

    </div>
@endsection

@section('script')
    <!-- slick slider -->
    <script type="text/javascript" src="{{ asset('plugins/slick/slick.min.js') }}"></script>
    <!-- page js -->
    <script type="text/javascript" src="{{ asset('js/'.strtolower(Route::currentRouteName()).'.js?v='.$version) }}"></script>
@endsection
